pragma solidity 0.5.2;

import "./OwnedMortal.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";


/// @title MemberOrganization
contract MemberOrganization is Mortal {
    using SafeMath for uint256;

    event NewMember(address member);
    event RemovedMember(address member, address by, bytes32 reason);
    event TransferredMembership(address by, address to);
    event TokenTransfer(address sender, address recipient, uint amount);
 
    /// @dev A modifier to restrict a function to members
    modifier onlyMembers {
        require(memberIDs[msg.sender] != 0, "Only can be called by a member");
        _;
    }

    /// @dev A modifier that requires the given address to be a member
    modifier isAMember(address test) {
        require(memberIDs[test] != 0, "Address given is not a member");
        _;
    }

    struct Member {
        address member;
        uint256 balance;
        uint256 whiskeyBucks;
    }

    /// @notice Holds the connection of addresses to the index into members
    mapping(address => uint) public memberIDs;
    /// @notice The array of members
    Member[] public members;
    /// @notice The number of members
    uint256 public memberCount;

    /// @dev Have to a fake-member to make the id checks work
    constructor() public {
        addMember(address(0));
    }

    /// @notice Adds a new member to the organization
    /// @param newMember The new member"s address
    function addMember(address newMember) public onlyOwner {
        uint256 id = memberIDs[newMember];
        if (id == 0) {
            memberIDs[newMember] = members.length;
            id = members.length++;
        }
        if (newMember != address(0)) memberCount++;
        members[id] = Member({member: newMember, balance: 0, whiskeyBucks: 0}); 
        emit NewMember(newMember);
    }

    /// @notice Removes a member from the organization
    /// @dev Shifts the target member to the end of members to ensure ids always increment
    /// @param targetMember The address of the member to remove
    /// @param why The reason why they are being removed
    function removeMember(address targetMember, bytes32 why)
        public
        onlyOwner
        isAMember(targetMember)
    {
        require(
            targetMember != address(0),
            "Cannot remove the 0x0 starting address"
        );

        // Move the target member to the end of the members array
        for (uint i = memberIDs[targetMember]; i < members.length - 1; i++) {
            members[i] = members[i + 1];
            memberIDs[members[i].member] = i;
        }
        memberIDs[targetMember] = 0;
        delete members[members.length - 1];
        members.length--;
        
        emit RemovedMember(targetMember, msg.sender, why);
    }

    /// @notice Transfer some of your tokens to someone else
    /// @param recipient The address of the member who will receive the tokens
    /// @param amount The amount of tokens to transfer
    function transferToken(address recipient, uint256 amount)
        public
        onlyMembers
    {
        Member storage sender = members[memberIDs[msg.sender]];
        Member storage receiver = members[memberIDs[recipient]];
        require(
            sender.balance >= amount,
            "You do not have this amount of tokens"
        );
        sender.balance -= amount;
        receiver.balance += amount;
        emit TokenTransfer(sender.member, recipient, amount);
    }

    /// @notice Transfer your membership to another address
    /// @param newMember The new address to transfer membership to
    function transferMembership(address newMember) public onlyMembers {
        emit TransferredMembership(msg.sender, newMember);
        Member storage m = members[memberIDs[msg.sender]];
        m.member = newMember;
    }
}
