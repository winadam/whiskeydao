pragma solidity 0.5.2;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "./BountyProgram.sol";


/// @title WhiskeyDAO
contract WhiskeyDAO is BountyProgram {
    using SafeMath for uint256;


    event BountyRewardPoolDonation(address member, uint amount);
    event BountyRewardPoolCashout(address member, uint amount);

    event NewFinanicalManager(address oldManager, address newManager);

    event TokenCreated(address recipient, address signer, uint amount);
    event TokenDestroyed(address owner, address signed, uint amount);

    /// @notice The current finance manager, signs off on generating tokens
    address public financeManager;

    /// @notice The pool of money that backs the whiskey bucks
    uint256 public bountyRewardPool;

    /// @param _financeManager The starting finanical manager
    /// @param _requiredAuthorizationPercentage The percentage of members required to authorize bounties
    constructor(
        address _financeManager,
        uint256 _requiredAuthorizationPercentage
    ) BountyProgram(_requiredAuthorizationPercentage)
    public {
        require(_financeManager != msg.sender, "Finance manager cannot be the owner");
        financeManager = _financeManager;
        owner = msg.sender;

        addMember(owner);

        emit NewFinanicalManager(address(0), financeManager);
        addMember(financeManager);

        bountyRewardPool = 0;

    }

    /// @notice Make a different member the finanical manager
    /// @param newManager The address of the new manager
    function transferFinanceManager(address newManager) public onlyMembers isAMember(newManager) {
        require(msg.sender == financeManager || msg.sender == owner, "Can only be done by the current manager or the owner.");
        emit NewFinanicalManager(financeManager, newManager);
        financeManager = newManager;
    }

    /// @notice Mints tokens and requires a signature
    /// @param recipient Where the tokens are going
    /// @param amount How many tokens to create
    /// @param v Part of the signature that signed the transaction
    /// @param r Part of the signature that signed the transaction
    /// @param s Part of the signature that signed the transaction
    function createToken(
        address recipient,
        uint256 amount,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) public onlyMembers isAMember(recipient) {
        address signed = checkSignature(recipient, amount, v, r, s);
        Member storage m = members[memberIDs[recipient]];
        m.balance += amount;

        emit TokenCreated(recipient, signed, amount);
    }

    /// @notice Removes tokens and requires a signature
    /// @param recipient Where the tokens are coming from
    /// @param amount How many tokens to remove
    /// @param v Part of the signature that signed the transaction
    /// @param r Part of the signature that signed the transaction
    /// @param s Part of the signature that signed the transaction
    function cashoutToken(
        address recipient,
        uint256 amount,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) public onlyMembers isAMember(recipient) {
        address signed = checkSignature(recipient, amount, v, r, s);
        Member storage m = members[memberIDs[recipient]];
        m.balance -= amount;
        
        emit TokenDestroyed(recipient, signed, amount);
    }

    /// @notice Donate to the bounty reward pool
    /// @param amount The amount of tokens to send to the pool
    function donateToBounty(uint256 amount) public onlyMembers {
        Member storage m = members[memberIDs[msg.sender]];
        require(m.balance >= amount, "Not enough money");
        m.balance -= amount;
        bountyRewardPool += amount;

        emit BountyRewardPoolDonation(msg.sender, amount);
    }

    /// @notice Converts whiskey bucks into tokens using the reward pool
    /// @dev A member can redeem their share of the whiskeys bucks for tokens
    /// @param amount How many whiskey bucks to convert
    /// @return The number of tokens you are receiving
    function convertBountyRewards(uint256 amount)
        public
        onlyMembers
        returns (uint256 entitled)
    {
        Member storage m = members[memberIDs[msg.sender]];
        require(m.whiskeyBucks >= amount, "Not enough whiskey bucks");

        entitled = (amount * bountyRewardPool) / totalWhiskeyBucks;
        require(bountyRewardPool >= entitled, "Error in converting math!");

        m.balance += entitled;
        bountyRewardPool -= entitled;

        m.whiskeyBucks -= amount;
        totalWhiskeyBucks -= amount;

        emit BountyRewardPoolCashout(msg.sender, amount);

        return entitled;
    }

    /// @dev Checks if signature is valid and signed by the correct person
    function checkSignature(
        address recipient,
        uint256 amount,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) internal view returns (address signerAddress) {
        Member storage m = members[memberIDs[recipient]];
        bytes32 hash = keccak256(
            abi.encodePacked(recipient, m.balance, amount)
        );
        signerAddress = ecrecover(hash, v, r, s);
        if (recipient == financeManager) {
            require(
                signerAddress == owner,
                "FinanceManager needs a signature from the Owner!"
            );
        } else {
            require(
                signerAddress == financeManager,
                "Requires a valid signature from the FinanceManager"
            );
        }
        return signerAddress;
    }
}
