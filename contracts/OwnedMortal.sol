pragma solidity 0.5.2;


contract Owned {
    address payable public owner;

    modifier onlyOwner {
        require(msg.sender == owner, "Only can be called by the owner");
        _;
    }

    event OwnershipTransfer(address oldOwner, address newOwner);

    constructor() public {
        owner = msg.sender;
        emit OwnershipTransfer(address(0), owner);
    }

    function transferOwnership(address payable newOwner) public onlyOwner {
        emit OwnershipTransfer(owner, newOwner);
        owner = newOwner;
    }
}


contract Mortal is Owned {
    function destory() public onlyOwner {
        selfdestruct(owner);
    }
}
