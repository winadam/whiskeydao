pragma solidity 0.5.2;

import "./MemberOrganization.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";


/// @title BountyProgram
contract BountyProgram is MemberOrganization {
    using SafeMath for uint256;

    event NewBounty(uint bountyID);
    event BountyAuthorization(uint bountyID, address member);
    event BountyApproval(uint bountyID, address approver, bool support);
    event BountyPayout(uint bountyID);

    /// @notice The storage for all bounties
    BountyReward[] public rewards;

    /// @notice The percentage of members that must authorize a bounty
    uint256 public requiredAuthorizationPercentage;

    /// @notice The total number of whiskey bucks
    uint256 public totalWhiskeyBucks;

    struct BountyReward {
        
        bytes32 name;
        bytes32 description;

        address[] assignedMembers;
        uint256 numberOfAssignedMembers;

        address[] approvers;
        uint256 numberOfApprovers;

        mapping(address => bool) approvalVotes;
        uint256 approvalCount;

        address[] authorizations;
        uint256 authorizationCount;
        mapping(address => bool) authorizationVoted;

        uint256 whiskeyBucksReward;
        bool complete;
    }


    constructor(uint256 _requiredAuthorizationPercentage) public {
        require(
            _requiredAuthorizationPercentage >= 0 &&
            _requiredAuthorizationPercentage <= 100,
            "Required authorization percentage must be between 0 and 100"
        ); 
        requiredAuthorizationPercentage = _requiredAuthorizationPercentage;
    }

    /// @notice Creates a bounty proposal that if authorized and approved, 
    ///         will create and give the assigned members whisky bucks
    /// @dev This should be looked at to make sure the struct is initalized correctly
    /// @param assignedTo The members that should complete the bounty and will be payed
    /// @param approvedBy The members that have to approve the final work for payment
    /// @param amount The amount of whiskey bucks to create and give the assigned members on completion
    /// @return The ID for the newly created bounty
    function createBountyReward(
        bytes32 name,
        bytes32 description,
        address[] memory assignedTo,
        address[] memory approvedBy,
        uint256 amount
    ) public onlyMembers returns (uint bountyID) {
        emit NewBounty(bountyID);

        bountyID = rewards.length++;
        BountyReward storage br = rewards[bountyID];
        br.assignedMembers = assignedTo;
        br.numberOfAssignedMembers = assignedTo.length;
        br.approvers = approvedBy;
        br.numberOfApprovers = approvedBy.length;
        br.whiskeyBucksReward = amount;
        br.name = name;
        br.description = description;
        return bountyID;
    }

    /// @notice Puts a vote towards authorizing a bounty
    /// @dev Member can not take back their vote to avoid someone authorizing work, 
    ///      they do the work, and rescending the authorization
    /// @param bountyID The bounty you would like to authorize
    function authorizeBounty(uint bountyID) public onlyMembers {
        BountyReward storage br = rewards[bountyID];
        require(
            br.authorizationVoted[msg.sender] != true,
            "Already authorized this bounty!"
        );
        br.authorizationVoted[msg.sender] = true;
        br.authorizations[br.authorizations.length++] = msg.sender;
        br.authorizationCount++;

        emit BountyAuthorization(bountyID, msg.sender);
    }

    /// @notice Approve or disapprove a bounty"s completion
    /// @dev Any member can authorize a bounty, but only the designated approvers are counted
    /// @param bountyID The bounty you would like to approve
    /// @param support Your approval or disapproval
    function approveBounty(uint bountyID, bool support) public onlyMembers {
        rewards[bountyID].approvalVotes[msg.sender] = support;
        rewards[bountyID].approvalCount++;

        
        emit BountyApproval(bountyID, msg.sender, support); 
    }

    /// @notice Pays out a bounty if the conditions are met
    /// @dev Make sure the conditions are correct
    /// @param bountyID The bounty to pay out
    function payoutBounty(uint bountyID) public {
        BountyReward storage br = rewards[bountyID];

        require(
            br.complete == false,
            "This bounty has already been payed out!"
        );

        // Enough people had to authorize the bounty
        require(
            ((br.authorizationCount * 100) / memberCount) >= requiredAuthorizationPercentage,
            "Not enough members have authorized this bounty"
        );

        // The approvers had to approve that the bounty was completed
        // Approval count does not mean the only approvals are from the designated approvers
        require(br.approvalCount >= br.approvers.length, "Not enough approvers approved this bounty");
        for (uint i = 0; i < br.approvers.length; i++) {
            address approver = br.approvers[i];
            require(
                br.approvalVotes[approver],
                "Missing an approver's approval"
            );
        }

        // Payout to members
        for (uint i = 0; i < br.assignedMembers.length; i++) {
            Member storage m = members[memberIDs[br.assignedMembers[i]]];
            m.balance += br.whiskeyBucksReward;
        }
        br.complete = true; // Prevent paying out twice
        
        emit BountyPayout(bountyID);
    }


}
